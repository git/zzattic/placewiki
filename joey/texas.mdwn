I've been in Texas three times.

First, we went through it on Amtrak, on the [[SouthWestCheif]] line.
Notable moments include crossing the Rio Grande (not so grande in my
opinion), stopping once in sigt of a shanty town of plywood and boxes
across the [[Mexico]] border, and being delayed for hours and hours next to
some kind of stockyard. The rest is a total blur or blah.

Next time I flew through the [[Dallas]] airport, changed planes, just
another airport, don't remember much.

Third time we were driving accross from California, and went all accross it
in one day from [[NewMexico]]. I remember: Lots of pickup trucks and hats,
lots of ads for steakhouses. Toward the [[Oaklahoma]] border I liked the
country a bit more.

All in all, a state I prefer to avoid, or would rather take in small doses,
but which only seems available in far too large doses.
