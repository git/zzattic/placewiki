Tegucigalpa was my first really foreign city to visit. The main places I
know in Tegus are the [[Airport]], around the [[StadiumArea]], and the
[[DownTownSquare]].

It's a hilly city in a partial bowl, that sorta trails off into pre/post
urban wasteland out toward the airport. I have no idea how to get to the
airport from anything else, but on my many trips to and from there, they
never took the same route twice, and the route always involved twisty not
major roads through random slices of Honduran life.

It's suprisingly easy to get lost walking around Tegus, you start up a
street up a hill and get turned around, and come down somewhere completly
different pointed a different direction. I never got an overall sense of
the whole geography of the city, just bits and peices, and a white stone
angel up there on the hill over it all.

There's also a honking huge amazingly echoing cathederal, for Honduras's
patron saint (Our Lady of Suyapa). I was there for the first few minutes
of a big ceremony before I had to leave for the airport. If you look down
the big hill this enormous cathederal is on, you'll find the worst slum
I saw in all of [[Honduras]].

Paths: [[Tegucigalpa-SanPedroSula]], [[Tegucigalpa-Guaimaca]]
