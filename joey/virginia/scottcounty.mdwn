Scott Country, [[Virginia]] is where I grew up until 5th grade or so when we sold the [[OldFarm]] and moved to [[Bristol]].

It's a rural county in SW [[Virginia]], [[GateCity]] is the largest city as far as I know; [[Mendota]] and [[Hiltons]] are two other little towns that I know reasonably well. I don't know the borders that well, except in places, like where the county (and state) line is on the road to the [[Farm]]. The parts I know best are around the [[OldFarm]] and [[Store]] where I grew up. I probably know most of the area on the southern side of the county, but on the other side of the [[ClinchMountain]] I don't know the land very well and don't know where the county ends.

Starting at the state line the general lay of the land is rolling hills with a couple long valleys, including [[RichValley]], and then flat land around the North Fork of the [[HolstonRiver]], then
[[ClinchMountain]].