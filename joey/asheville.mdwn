Asheville is a city I always seem to pass through, but never visit. Maybe
this is why I don't much like it. 

It's a city on a river, in the foothills of the [[NorthCarolina]]
mountains, and probably quite pretty. It's becoming more and more of a
tourist attraction and a cultural center for this area with a big music
scene, lots of other crunchy stuff, lots of attractions for people who
might otherwise end up feeling cut off and stuck in this area and who
migrate to Ashveille instead.

Unfortunatly for me it's mostly a place to fly through too quickly on
highways that are badly signed, past the apparently permanent busloads of
prisoners working roadside trash pickup, over the river on a bridge where
you have to merge three lanes at a go and never have time to see any of the
sights. The only time I've gotten near to downtown was when I missed the
merge and got lost, and then the city just kept chewing me up and spitting
me out on yet another large 4-lane on another side of town, going the wrong
way.
