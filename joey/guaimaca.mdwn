Guaimaca is named after, I think, an Indian guy and his hammock. It's a
generally unimportant town in [[Honduras]], and it's where I spent most of my
time in the country. It's in a wide valley.

[[Guaimaca]] is awash in various religious groups. On the outskirts of
town, down the highway toward Tegucigalpa, there's the compound of the
Mnenonites, a nice place to go for some food or a potted plant. Up on the
other side of town is an orphanage run by some other religion, and I
remember one or two other religious compounds. And right in the middle of
it, right off the central square, is the Catholic [[Church]]. This all took
a while to figure out because from the beginning of my time there, I was
pretty much enmeshed in the Church (and me not religious, let alone
Catholic).

The interesting bits of town are mostly along one main road that comes down
from the highway to the square. People are constantly biking up and down
it, frequently whole families on one bike. On the other side of the square
from the church is a little restaurant where I had my first plato typico,
with a way too loud pool hall next door. And kittycorner across from the
church is the [[IceCreamShop]], which we visited religiously.

Follow the main road past the square and on, and you come to a creek, and a
ford where herds of cattle are brought to drink. Guaimaca pretty much ends
there, and on down this road was the first place we explored. It continues
along past a few houses, a pond on the left with cows, and then turns up
into the hills. I went off the left side of the road once and explored
around in the lower hills, it was dry country (well, dry season then), but
enjoyable. Follow the road up to the top and there's a branch off that goes
down to the dump. 

We actually went down there too, possibly because it was't clear that was
its destination. That's quite something to see, I think it was the trash
fires in the dump that tipped me off again that I wasn't back home. And the
feral dogs.

If instead you walk down the far side of the hills, it's very pretty
country, with some nice farms scattered along. Easy to walk far enough
along that going back over the hills becomes a pain.

Ride out from town a little way at night, on a borrowed bicycle, and
amazingly soon you can't see where you're going at all, you lose the road
and have to walk. The sky is huge, full of stars. There's no sky glow, it's
better than being on the [[OuterBanks]]. I want to go back there just for
that, but not _just_ for that.

Paths: [[Tegucigalpa-Guaimaca]], [[Guaimaca-Orica]], [[Guaimaca-ElChile]]
