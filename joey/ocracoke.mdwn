A narrow spit of sand between the gentle sound and the tearing ocean which
keeps trying to wash it away. One highway down it, beside the (artificial
it turns out) sand dunes, with scrub trees and grass and sand. On hot days,
mirages appear down the road, flickering water. The game is to try to find
the first glimpse of the real ocean waves through the latest hurracane made
gap in the dunes.

Starting at the [[HatterasFerry]], there is the path to the
[[ChannelDregeDump]], then the [[PonyPen]], then creeks.. Shad Hole creek
is the name I remember the best. Each with its little bridge and "Bridge
Ices before Road" sign. Then a bit of a pine forest as the island widens a
little, and the [[Campground]] is on the left. The road continues down past the
[[LifeGuardBeach]] and [[Airport]] to [[Town]].

The beach at [[Ocracoke]] goes on and on in both directions as far as I can
see, with gentle curves in and out. Waves break at different places up and
down the beach, all the way out to the horizon. Notable beaches include the 
[[CampgroundBeach]], [[LifeGuardBeach]], and [[VeryWideBeach]]. Also, I hear,
a [[NudeBeach]] but I've never been there.

Paths: [[HatterasFerry]], [[SwanQuarterFerry]], [[CedarIslandFerry]]
