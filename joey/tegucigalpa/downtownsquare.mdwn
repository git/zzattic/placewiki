There's more to [[DownTown]] than the central square and attached
pedestrian mall, but this is its core. The square itself is notable for
being a no sitting zone. Almost anywhere you'd like to sit down, that is
not already occupied by a street vendor, is covered in spikes. That's most
of the walls around things in the square, etc. There are a few exceptions,
and these get covered solid with people.

Another notable feature is The Line. This starts at the building in the
middle of the western side of the square, and goes straight back about
halfway to the middle. I never bothered to figure out what it was a line
for (theater or something? government office?), it somehow just didn't
interest me at all, except as an obstacle to be navigated in the square.

There are some fenced off things inside the square, I think one is a
fountain, the rest I don't remember. Not interesting, the interesting thing
to do there is to watch the people, and to try to blend in. Not easy for a
big bearded American, but I apparently managed it, since after a while
tourists seemed just as likely to take a picture of me as of anything else.

I should mention that this was all experienced in my
oh-god-I'm-in-a-foreign-country-and-I-seem-to-have-utterly-forgotten-the-language-let's-be-a-deaf-mute
stage of being in [[Honduras]]. The first couple of days.

Best food in the square is from the street vendors on the northern edge of it.
Sliced green mangos in baggies are excellent in January (don't go for the
salt or chiles or whatever they put on it). And fresh-cooked food from the
carts. There's also a smoothie place to the right of The Line. Back on the
other side of the square are American fast food places and an interior
mall, all avoided.

There's also the pedestrian mall, which streches for quite a lot of blocks
west of the square. Lots of tables of assorted, mostly very cheap stuff.
Some excellent bakeries, a little grocery store off to the left of it a few
blocks down, and an internet cafe on the left down near the end.

The whole east side of the square fronts on a small cathederal, which is
interesting inside if you like that kind of thing.
