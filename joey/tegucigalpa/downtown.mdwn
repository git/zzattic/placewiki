Besides the [[DownTownSquare]], there's a fairly interesting museum of
Honduran art, there's a very cheap and ultra-basic hotel that we stayed at,
and there's a restaurant with a name I forgot that has a serious and famous
shrine in the bathroom. There are a lot of very low electric poles with
rats nests of lines that I often had to duck under. There are some
interesting narrow back streets too with little restaurants and stuff on
them.

Paths: [[StadiumArea-DownTown]]
