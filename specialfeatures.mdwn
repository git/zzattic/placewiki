This wiki has some [[SpecialFeatures]] to make it well suited to descibing
places, and save you some bother.

* [[CrossReferences]] are automatically made between pages by different
  users that describe the same place. (TODO)

* [[Paths]] between places can be described by making a page named like
  StartPlace-EndPlace. The path will automatically be listed on both pages.
  (TODO)

* [[BackLinks]] are tracked and displayed to make it easy to find other
  pages that refer to each page.

* Links are case insensative, so you don't have to worry about capitalising
  things perfectly all the time.

* [[PageStructure]] allows grouping related pages -- and lets each user
  have their own directory to describe their own places.

* [[AutoMap]] builds maps of the relationships between places on the site
  based on what pages link to others and on [[PageStructure]]. (TODO)
