Please feel free to edit this [[wiki]], add yourself to the list of people 
in the [[index]], and describe all the places you've been.

It's a good idea to try to use the same [[PageNames]] and [[PageStructure]] for
pages describing places as other people have on the wiki, since this allows
[[CrossReferences]] between different people's descriptions of a place.

This wiki has some [[SpecialFeatures]] to make it well suited to descibing
places, and save you some work.
