This wiki allows grouping pages in subdirectories, and uses more
subdirectories than most wikis, since each user gets one of their own for
their own descriptions on places.

The basic structure of pages on the wiki is: YourName/PlaceName. If you're
describing something in that place, like a building in a city, you can use
a third level: YourName/PlaceName/Detail. Generally the structure is kept
pretty flat though; rather than
YourName/NorthAmerica/California/SanFransisco/MarketStreet use just
YourName/SanFransisco/MarketStreet, or even YourName/MarketStreet, since
it's "the" Market Street.

If you want to describe a path between two places, use the form
StartPlace-EndPlace, and put the page in the same directory as the two
pages it's describing (if possible). It doesn't matter which order the
start and end places are listed.

If in doubt, search to see what structure other users are using and
duplicate it. But don't be afraid to create your own structures for places,
or ways of looking at places, that are unique to you.
